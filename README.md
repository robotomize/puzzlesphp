
# README #

Slightly different brainteasers on PHP

### What is this repository for? ###

* Simple string compressor
* Simple user generator
* Dealer shuffling cards
* INI configuration parser in an associative array
* Pi - generator class
* Pi - Find the offset sequence number in Pi.
* CaesarCipher RU create and crack ciphers
* Simple binary search in 20min

### How set up ###
* git clone https://github.com/robotomize/puzzlesPHP.git
* install composer
* composer update

### Examples of classes of puzzles ###
#### This is an example of the work class to parse ini config ####
```php
<?php
    namespace ConfigParser;

    include 'ConfigParser.php'; // or use autoload

    /**
     * pars_task.ini This configuration file section
     */

    $tt = new ConfigParser('pars_task.ini');
    $resultArray = $tt->getResultArray();

    /**
     * View final array section
     */
    print_r($resultArray);

```

### The following example, a line of compression puzzle with repeated characters ###
#### Example with factory class ####
```php
<?php

namespace CompressionOverCounter;

include 'CompressFactory.php';

$myLongString = 'My fuuuuzyyyywuuuuuzyyyyy it so looooong';
print CompressFactory::makeCompressOverCounter($myLongString);

```

### The binary search in 20min ###
#### Simple binary search####
```php
<?php

namespace FiveMinBinarySearch;

use FiveMinBinarySearch\FiveMinBinarySearch;

include 'FiveMinBinarySearch.php';

$tt = new FiveMinBinarySearch('372363105623559049375267496732', 5); // matching 5
print $tt . PHP_EOL;
print $tt->start();

```


