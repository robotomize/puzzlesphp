<?php

namespace FiveMinBinarySearch;

/**
 * Class FiveMinBinarySearch
 * @package FiveMinBinarySearch
 * @author robotomize@gmail.com
 * @version fast
 */
class FiveMinBinarySearch
{
    /**
     * @var array
     */
    private $myInputArray = [];

    /**
     * @var
     */
    private $searchingElement;

    /**
     * @return mixed
     */
    public function getSearchingElement()
    {
        return $this->searchingElement;
    }

    /**
     * @param mixed $searchingElement
     */
    public function setSearchingElement($searchingElement)
    {
        $this->searchingElement = $searchingElement;
    }

    /**
     * @param $sequence
     * @param $searchingElement
     */
    public function __construct($sequence, $searchingElement)
    {
        if ($sequence == '' || $searchingElement == '') {
            throw new \InvalidArgumentException;
        } else {
            $this->sequence = $sequence;
            $this->searchingElement = $searchingElement;
            $this->myInputArray = str_split($this->sequence);
            $this->sortingData();
        }
    }

    /**
     * @return array
     */
    public function getMyInputArray()
    {
        return $this->myInputArray;
    }

    /**
     * @param array $myInputArray
     */
    public function setMyInputArray($myInputArray)
    {
        $this->myInputArray = $myInputArray;
    }

    /**
     * @var string
     */
    private $sequence = '';

    /**
     * @param string $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $this->myInputArray = str_split($this->sequence);
        $this->sortingData();
    }

    /**
     * @var array
     */
    private $outputArray = [];

    /**
     * @return array
     */
    public function getOutputArray()
    {
        return $this->outputArray;
    }

    /**
     * @return array
     */
    private function sortingData()
    {
        for ($j = 0; $j < count($this->myInputArray); $j++) {
            for ($i = 1; $i < count($this->myInputArray) - $j; $i++) {
                if ($this->myInputArray[$i] < $this->myInputArray[$i - 1]) {
                    $temp = $this->myInputArray[$i - 1];
                    $this->myInputArray[$i - 1] = $this->myInputArray[$i];
                    $this->myInputArray[$i] = $temp;
                }
            }
        }
    }

    /**
     * @var
     */
    private $resultValue;

    /**
     * @return mixed
     */
    public function getResultValue()
    {
        return $this->resultValue;
    }

    /**
     *
     */
    public function start()
    {
        return $this->run($this->myInputArray);
    }

    /**
     * @param $NArray
     *
     * @return string
     */
    private function run($NArray)
    {
        $firstIndex = (int)(count($NArray) / 2);

        if ($this->searchingElement > $NArray[$firstIndex]) {
            if ($this->searchingElement == $NArray[$firstIndex]) {
                $this->resultValue = $firstIndex;
                return $NArray[$firstIndex];
            } else {
                $this->run(array_slice($NArray, $firstIndex, count($NArray)));
            }
        } else {
            if ($this->searchingElement == $NArray[$firstIndex]) {
                $this->resultValue = $firstIndex;
                return $NArray[$firstIndex];
            } else {
                $this->run(array_slice($NArray, 0, $firstIndex));
            }
        }
        return 'Not found..';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->sequence !== '') {
            return $this->sequence;
        } else {
            return '';
        }
    }

    /**
     * @return string
     *
     * @throws \Exception
     */
    public function __invoke()
    {
        if ($this->sequence !== '' && 0 !== count($this->myInputArray)) {
            return $this->start();
        } else {
            throw new \Exception('Start parameters not found ');
        }
    }

}