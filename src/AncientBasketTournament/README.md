
# README #

## Ancient Basket Tournament ##
### Basic usage ###
```php
<?php

include 'AncientBasketTournament.php';
// constructor parameters 3 - number of throws the ball, 5 - The boundary set for the game
$tt = new AncientBasketTournament(3, 5);


/**
*  Further, the first parameter is the number of teams.
*  The second parameter, with a distance goal was scored. Depending on the distance points are added.
*  From the foul line (-1) - 1 point.
*  In between 1 and setting the border - 2 points. From a distance beyond the boundary line -3 points.
*
*/
$tt->setResultArray([1, 6], [2, 10], [1, 3]);
$tt->calcResult();
print $tt->getResultChamp() . PHP_EOL;
output: 5:3
```

