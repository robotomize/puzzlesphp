<?php

/**
 * Class AncientBasketTournament
 * @author robotomize@gmail.com
 * @version 1.1
 */
class AncientBasketTournament
{
    /**
     * @var int
     */
    private $countEntries = 0;

    /**
     * @var int
     */
    private $border = 0;

    /**
     * @var array
     */
    private $resultArray = array();

    /**
     * @param $countEntries
     *
     * @param $border
     */
    function __construct($countEntries, $border)
    {
        if (empty($countEntries) || empty($border)) {
            throw new \InvalidArgumentException;
        }

        if ($countEntries < 1 || $countEntries > 1000) {
            throw new InvalidArgumentException;
        }

        if ($border < 1 || $border > 30) {
            throw new InvalidArgumentException;
        }

        $this->countEntries = $countEntries;
        $this->border = $border;
    }

    /**
     * @var int
     */
    private $teamRed = 0;

    /**
     * @return int
     */
    public function getTeamRed()
    {
        return $this->teamRed;
    }

    /**
     * @var int
     */
    private $teamBlue = 0;

    /**
     * @return int
     */
    public function getTeamBlue()
    {
        return $this->teamBlue;
    }

    /**
     * @param $params
     */
    private function calcPoint($params)
    {
        $values = $params;
        switch($values[0]) {
            case 1:
                if ((int)$values[1] == -1) {
                    $this->teamRed += 1;
                    break;
                }
                if ((int)$values[1] > (int)$this->border) {
                    $this->teamRed += 3;
                } else {
                    $this->teamRed += 2;
                }
                break;
            case 2:
                if ((int)$values[1] == -1) {
                    $this->teamBlue += 1;
                    break;
                }
                if ((int)$values[1] > (int)$this->border) {
                    $this->teamBlue += 3;
                } else {
                    $this->teamBlue += 2;
                }
                break;
        }
    }

    /**
     * @return Exception|string
     */
    public function getResultChamp()
    {
        if (!empty($this->teamBlue) || !empty($this->teamRed)) {
            return sprintf('%s:%s', $this->teamRed, $this->teamBlue);
        } else {
            return new \Exception('Result doest finished');
        }
    }

    /**
     * Cleaning
     */
    private function cleanResult()
    {
        $this->teamBlue = 0;
        $this->teamRed = 0;
    }

    /**
     *  Main Calculator
     */
    public function calcResult()
    {
        if (empty($this->resultArray) || empty($this->border)) {
            return new \Exception('Result array is empty');
        }
        $this->cleanResult();
        foreach($this->resultArray as $values) {
            if ($values[0] > 2 || $values[0] < 1) {
                throw new InvalidArgumentException;
            }
            if ($values[1] < -1 || $values[1] > 50) {
                throw new InvalidArgumentException;
            }
            $this->calcPoint($values);

        }
    }

    /**
     * @return int
     */
    public function getCountEntries()
    {
        return $this->countEntries;
    }

    /**
     * @param int $countEntries
     */
    public function setCountEntries($countEntries)
    {
        $this->countEntries = $countEntries;
    }

    /**
     * @return int
     */
    public function getBorder()
    {
        return $this->border;
    }

    /**
     * @param int $border
     */
    public function setBorder($border)
    {
        $this->border = $border;
    }

    /**
     * @return array
     */
    public function getResultArray()
    {
        return $this->resultArray;
    }

    /**
     * @param array $resultArray
     */
    public function setResultArray($resultArray)
    {
        $this->resultArray = $resultArray;
    }

    /**
     * @return Exception|string
     */
    function __invoke()
    {
        if (!empty($this->teamBlue) || !empty($this->teamRed)) {
            return $this->getResultChamp();
        } else {
            $this->calcResult();
            return $this->getResultChamp();
        }
    }

    /**
     * @return Exception|string
     */
    function __toString()
    {
        if (!empty($this->teamBlue) || !empty($this->teamRed)) {
            return $this->getResultChamp();
        } else {
            $this->calcResult();
            return $this->getResultChamp();
        }
    }
}