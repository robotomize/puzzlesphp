<?php

include 'AncientBasketTournament.php';

$fh = fopen('php://stdin','r') or die($php_errormsg);
$i = 0;
$firstParameters = array();
$secondParameters = array();
while($s = fgets($fh,1024)) {

        if ($i == 0) {
            $firstParameters = explode(' ', $s);
        } else {
            if ($i < $firstParameters[0] || empty($firstParameters)) {
                $temp = explode(' ', $s);
                $secondParameters[] = array($temp[0], $temp[1]);
            } else {
                $temp = explode(' ', $s);
                $secondParameters[] = array($temp[0], $temp[1]);
                break;
            }
        }
    $i++;
}

$tt = new AncientBasketTournament($firstParameters[0], $firstParameters[1]);
$tt->setResultArray($secondParameters);
$tt->calcResult();
print $tt->getResultChamp() . PHP_EOL;
