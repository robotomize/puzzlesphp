<?php

namespace Pi;

/**
 * Change this option
 */
ini_set('memory_limit', '64024M');

use Pi\PiFinder;
use Pi\PiFindBillion;

require_once 'PiFinder.php';
require_once 'PiFindBillion.php';
/**
$tt = new PiFinder('/Users/robotomize/Desktop/pi-10million.txt', '2148400');
print $tt->run();
*/

$tt = new PiFindBillion('/Users/robotomize/Desktop/pi-billion.txt', '2148400');
print $tt->run() . PHP_EOL;
print $tt->getCountMatching() . PHP_EOL;
print_r($tt->getCountMatchingArray());
