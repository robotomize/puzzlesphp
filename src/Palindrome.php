<?php

/**
 * Class Palindrome
 */
class Palindrome
{
    public function is_palindrome($num) {
        return $num == strrev($num);
    }

    public function solution($num) {
        $count = $i = 0;
        while($count<$num) {
            $i++;
            if ($this->is_palindrome($i) && $this->is_palindrome(decbin($i))){
                $count++;
            }
        }
        return $i;
    }
}