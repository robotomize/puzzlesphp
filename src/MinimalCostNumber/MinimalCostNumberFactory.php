<?php

namespace numbers;

use numbers\MinimalCostNumber;

require_once 'MinimalCostNumber.php';

/**
 * Class MinimalCostNumberFactory
 * @package MinimalCostNumber
 */
class MinimalCostNumberFactory
{
    public static function makeMinCostNumber($seq)
    {
        return new MinimalCostNumber($seq);
    }
}