<?php

class MinimalSeq
{
    /**
     * @param $seq
     *
     * @throws \Exception
     */
    public function __construct($seq)
    {
        if (!empty($seq)) {
            $this->noOrderSeq = $seq;
            $this->seqArray = str_split($seq);
        } else {
            throw new \Exception('sequence is empty');
        }
    }

    /**
     * @var array
     */
    private $directMatchVector = array();

    /**
     * @return array
     */
    public function getDirectMatchVector()
    {
        return $this->directMatchVector;
    }

    /**
     * @var int
     */
    private static $size = 8;

    /**
     * @return int
     */
    public static function getSize()
    {
        return self::$size;
    }

    /**
     * @param int $size
     */
    public static function setSize($size)
    {
        self::$size = $size;
    }

    /**
     * @return string
     */
    private function calculateMinSeq()
    {
        $rightInt = 4;
        $leftInt = 0;

        $currentIndex = self::$size - $rightInt;

        for ($i = self::$size - $rightInt;  $i < self::$size; --$i)
        {
            if ($i < $leftInt)
            {
                $this->directMatchVector[] = $this->seqArray[$currentIndex];
                $leftInt = ++$currentIndex;
                $i = self::$size - (--$rightInt);
                $currentIndex = $i;
                if (!$rightInt) {
                    break;
                }
            }

            if ($this->seqArray[$i] <= $this->seqArray[$currentIndex]) {
                $currentIndex = $i;
            }

        }
        $this->resultSeq = implode('', $this->directMatchVector);
        return $this->resultSeq;
    }

    /**
     * @var string
     */
    private $resultSeq = '';

    /**
     * @return string
     */
    public function getResultSeq()
    {
        return $this->resultSeq;
    }

    /**
     * @var int
     */
    private static $numSeq = 4;

    /**
     * @return int
     */
    public static function getNumSeq()
    {
        return self::$numSeq;
    }

    /**
     * @param int $numSeq
     */
    public function setNumSeq($numSeq)
    {
        self::$numSeq = $numSeq;
        $this->seqArray = str_split(self::$numSeq);
    }

    /**
     * Main calc method
     *
     * @return string
     */
    public function run()
    {
        return $this->calculateMinSeq();
    }

    /**
     * @var array
     */
    private $seqArray = array();

    /**
     * @return array
     */
    public function getSeqArray()
    {
        return $this->seqArray;
    }

    /**
     * @var
     */
    private $noOrderSeq;

    /**
     * @return mixed
     */
    public function getNoOrderSeq()
    {
        return $this->noOrderSeq;
    }

    /**
     * @param mixed $noOrderSeq
     */
    public function setNoOrderSeq($noOrderSeq)
    {
        $this->noOrderSeq = $noOrderSeq;
    }

    /**
     * @return $this|string
     */
    public function __invoke()
    {
        if (!empty($this->resultSeq)) {
            return $this->resultSeq;
        } else {
            if (0 !== count($this->seqArray)) {
                return $this->run();
            } else {
                return $this;
            }
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (!empty($this->resultSeq)) {
            return $this->resultSeq;
        } else {
            if (0 !== count($this->seqArray)) {
                return $this->run();
            } else {
                return serialize($this);
            }
        }
    }
}
