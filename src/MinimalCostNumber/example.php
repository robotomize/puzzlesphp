<?php

namespace minimal;

use numbers\MinimalCostNumber;

include 'MinimalCostNumber.php';

$line = trim(fgets(STDIN));

$tt = new MinimalCostNumber($line);
print $tt->run() . PHP_EOL;

