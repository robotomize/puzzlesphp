<?php

/**
 * abstract compress class
 *
 * @author robotomize@gmail.com
 */

namespace CompressionOverCounter;

/**
 * Class AbstractCompress
 *
 * @package CompressionOverCounter
 */
abstract class AbstractCompress
{
    /**
     * @return mixed
     */
    abstract public function compress();
}