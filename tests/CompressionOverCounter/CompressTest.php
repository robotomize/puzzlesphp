<?php

namespace test;

require_once '../../src/CompressionOverCounter/CompressFactory.php';
require_once '../../src/CompressionOverCounter/CompressionOverCounter.php';

/**
 * Class CompressTest
 *
 * @package test
 * @author robotomzie@gmail.com
 * @usage simple compress test
 */
class CompressTest extends \PHPUnit_Framework_TestCase
{
    public function testCommonCompress()
    {
        $this->assertEquals('hd7g4sajsjaksj4slsl1sl1slsl1slsklkfdkfkf',
            \CompressionOverCounter\CompressFactory::makeCompressOverCounter('hddddddddggggg
            sajsjaksjjjjjslsllsllslsllslsklkfdkfkf')->compress());

        $this->assertEquals('g9l1k1l1k1l1k1l3ksksksk1s1k5js',
            \CompressionOverCounter\CompressFactory::makeCompressOverCounter('ggggggggggllkkllkkll
            kkllllkskskskksskkkkkkjs')->compress());

        $this->assertEquals('hd7g1dfjsfjsdk1dsdlsg2sajsjaksj4slsl1sl1slsl1slsklkfdkfkf',
            \CompressionOverCounter\CompressFactory::makeCompressOverCounter('hddddddddggdfj
            sfjsdkkdsdlsgggsajsjaksjjjjjslsllsllslsllslsklkfdkfkf')->compress());

        $this->assertEquals('hd6n8l4y3w2kdsjk1sksk1sd1g4sajsjaksj4slsl1sl1slsl1slsklkfdkfkf',
            \CompressionOverCounter\CompressFactory::makeCompressOverCounter('hdddddddnnnn
            nnnnnlllllyyyywwwkdsjkkskskksddgggggsajsjaksjjjjjslsllsllslsllslsklkfdkfkf')->compress());
    }
}
