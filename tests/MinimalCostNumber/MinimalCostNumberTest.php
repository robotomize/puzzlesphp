<?php

namespace test;

use numbers\MinimalCostNumberFactory;

include '../../src/MinimalCostNumber/MinimalCostNumberFactory.php';

/**
 * Class MinimalCostNumberTest
 * @package test
 * @author robotomzie@gmail.com
 * @version 0.1
 */
class MinimalCostNumberTest extends \PHPUnit_Framework_TestCase
{
    /**
     * true
     */
    public function testOnlyTrue()
    {

        $this->assertTrue(MinimalCostNumberFactory::makeMinCostNumber('12333333')->run() != '' ? true : false);
    }

    /**
     * false
     */
    public function testOnlyFalse()
    {
        $this->assertFalse(MinimalCostNumberFactory::makeMinCostNumber('12333333')->run() == 0 ? true : false);
    }

    /**
     * @group test1
     */
    public function testOnlyEquals()
    {
        $this->assertEquals('1233', MinimalCostNumberFactory::makeMinCostNumber('12333333')->run());
        $this->assertEquals('1111', MinimalCostNumberFactory::makeMinCostNumber('32111111')->run());
        $this->assertEquals('1111', MinimalCostNumberFactory::makeMinCostNumber('12131211')->run());
        $this->assertEquals('3323', MinimalCostNumberFactory::makeMinCostNumber('33333323')->run());
        $this->assertEquals('2313', MinimalCostNumberFactory::makeMinCostNumber('32333313')->run());
        $this->assertEquals('3331', MinimalCostNumberFactory::makeMinCostNumber('33333331')->run());
        $this->assertEquals('1111', MinimalCostNumberFactory::makeMinCostNumber('22112211')->run());
        $this->assertEquals('1111', MinimalCostNumberFactory::makeMinCostNumber('12312311')->run());
        $this->assertEquals('1112', MinimalCostNumberFactory::makeMinCostNumber('23131212')->run());
        $this->assertEquals('1123', MinimalCostNumberFactory::makeMinCostNumber('12332123')->run());
    }
}
